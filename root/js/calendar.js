/* I did not write this function credits to
Nano Ajax Library
(c) Schien Dong, Antradar Software Inc.
License: www.antradar.com/license.php
*/

function gid(d){
	var q = document.getElementById(d);
	return q;
}


function xmlHTTPRequestObject() {
	var obj = false;
	var objs = ["Microsoft.XMLHTTP","Msxml2.XMLHTTP","MSXML2.XMLHTTP.3.0","MSXML2.XMLHTTP.4.0"];
	var success = false;
	for (var i=0; !success && i < objs.length; i++) {
		try {
			obj = new ActiveXObject(objs[i]);
			success = true;
		} catch (e) { obj = false; }
	}

	if (!obj) obj = new XMLHttpRequest();
	return obj;
}




function showcalendar(month, year){
	if (month < 1){
		month = 12;
		year -= 1;
	}
	if (month > 12){
		month = 1;
		year += 1;
	}
	//macguyver's mod 12 function
	
	//console.log(month+' '+year);
	
	var rq = xmlHTTPRequestObject();
	rq.open('GET', 'functions/services.php?cmd=showcalendar&month='+month+'&year='+year, true);
	rq.onreadystatechange = function(){
		if (rq.readyState == 4){
			var res = rq.responseText;	
			document.getElementById('calendar').innerHTML = res;
		}
	}
	rq.send(null);
	
}

function listtasks(day, month, year){
	var rq = xmlHTTPRequestObject();
	rq.open('GET', 'functions/services.php?cmd=listtasks&day='+day+'&month='+month+'&year='+year, true);
	rq.onreadystatechange = function(){
		if (rq.readyState == 4){
			var res = rq.responseText;	
			document.getElementById('tasks').innerHTML = res;
		}
	}
	rq.send(null);
	
}

function addtask(day, month, year){
	var tasktime = parseInt(gid('tasktime').value);
	var apm = gid('taskapm').value;
	var taskdescription = gid('taskdescription').value;
	if (apm == 'pm') {tasktime = tasktime+1200;}
	
	//console.log(tasktime+taskdescription);
	
	var rq = xmlHTTPRequestObject();
	rq.open('GET', 'functions/services.php?cmd=addtask&day='+day+'&month='+month+'&year='+year+'&taskdescription='+taskdescription+'&tasktime='+tasktime, true);
	rq.onreadystatechange = function(){
		if (rq.readyState == 4){
			var res = rq.responseText;	
			document.getElementById('tasks').innerHTML = res;
		}
	}
	rq.send(null);
	
	
}

function deltask(taskid){
	if (!confirm('are you sure you want to delete this task?')) return;
	
	
	var rq = xmlHTTPRequestObject();
	rq.open('GET', 'functions/services.php?cmd=deltask&taskid='+taskid, true);
	rq.onreadystatechange = function(){
		if (rq.readyState == 4){
			var res = rq.responseText;	
			document.getElementById('tasks').innerHTML = res;
		}
	}
	rq.send(null);
	
}