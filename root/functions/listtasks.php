<?

function listtasks($day='', $month='', $year=''){
	global $db;
	global $userid;
	
	
	
	
	
	
	
	if ($day=='') $day = $_GET['day']+0;
	if ($month=='') $month = $_GET['month']+0;
	if ($year=='') $year = $_GET['year']+0;
	
	$date = mktime(0,0,0, $month+0, $day+0, $year+0);
	//echo $month.' '.$day.' '.$year;
	$upperbound = $date + 12*60*60; //forward twelve hours
	$lowerbound = $date - 12*60*60; //backcward twelve hours
	
	$query = "select * from schedules, tasks where schedules.userid = $userid and ((tasks.taskdate <= $upperbound) and (tasks.taskdate >= $lowerbound)) order by tasktime asc";
	//echo $query;
	$res = sql_query($query, $db);
	
	$date = date('l F jS, Y', mktime(0,0,0,$month, $day, $year));
	
	?>
	<div id="taskswrap">
		<div class="title"><h2>Tasks for <?echo $date;?></h2></div>
		<div class="itinerary">
			<?
			while ($myrow = sql_fetch_assoc($res)){
				
				$taskid = $myrow['taskid'];
				$tasktime = sprintf("%04s",$myrow['tasktime']+0);
				$tasktime = date("g:i a", strtotime($tasktime));
				$taskdescription = $myrow['taskdescription'];
				?>
				<div>
					<a class="del" onclick="deltask(<?echo $taskid;?>);"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="time"><?echo $tasktime;?></span><span class="description"><?echo $taskdescription;?></span>
				
				</div>
				<?
				
			}
			?>
		</div>
		
		<div class="subtitle">
			<i class="fa fa-pencil" aria-hidden="true"></i> Add tasks:
		</div>
		<div>
			<div class="inputrow">
				<label class="inputtitle">Description:</label><input id="taskdescription" type="text" class="">
			</div>
			<div class="inputrow">
				<label class="inputtitle">Time:</label>
				<select id="tasktime" class="">
					<?
					$i= 0;
					$c= 0;
					
					while($i<1200){
						
						$disptime = sprintf("%04s", $i);
						
						if($disptime<100){
						
							$disptime = '12:'.substr($disptime, -2);
						}
						else {
							$disptime = substr($disptime, 0,2).':'.substr($disptime, -2);
						}
						?>
						<option value="<?echo $i;?>"><?echo $disptime;?></option>
						<?	
					if ($c%2==0){$i+=30;}
					else $i+=70;
					$c++;
					}
					?>
				</select>
				
				<select id="taskapm">
					<option value="am">AM</option>
					<option value="pm">PM</option>
				</select>
			
				<span><button onclick="addtask(<?echo $day.','.$month.','.$year;?>);">Add</button></span>
			</div>
		</div>
		
		
	</div>	
	<?

}