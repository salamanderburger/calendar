<?
//$month = $_GET['month'];
//$year = $_GET['year'];

function showcalendar($month='', $year=''){
	
	global $userid;
	global $db;
	
	date_default_timezone_set('UTC');
	if ($month == '') {
		$currmonth = date('m');
		$month = $_GET['month'];
	}
	//echo $currmonth;
	if($year==''){
		$curryear = date('Y');
		$year = $_GET['year']+0;
	}
	
	$month = $month+0;
	$year = $year+0;
	
	//$month= 4;
	//$year= 2018; //test
	
	$april = 4;
	$days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	
	//echo 'current year is '.$year.' and the month is '.$month.' which has '.$days_in_month.' of days<br>';
	
	//if the month starts on sunday then go 7 days back
	//else, start calendar on first sunday before month begins
	
	$firstday = date('N', mktime(0, 0, 0, $month, 1, $year));
	
	$calendartitledisplay = date('F Y', mktime(0,0,0, $month, 1, $year));
	
	$last_sunday = $firstday%7;
	//if ($last_sunday==0) $last_sunday=7; //go 7 days back
	
	
	
	//echo $firstday.' is the first day of '.$april.' '.$curryear.'. last sunday was '.$last_sunday.' days ago.';
	
	$calstart = explode(",",date('d, m, Y', mktime(0,0,0, $month, 1-$last_sunday, $year)));
	$dateindex = $calstart;
	
	//echo $calstart[0].' '.$calstart[1].' '.$calstart[2].' '; 
	// calstart day month year
	
	//** find scheduling for this month ** //
	
	$query = "select * from schedules, tasks, users where users.userid = $userid and schedules.userid = users.userid and tasks.scheduleid = schedules.scheduleid";
	$res =  sql_query($query, $db);
	while($myrow = sql_fetch_assoc($res)){
		$schedulestart = $myrow['schedulestart'];
		$scheduledate = explode(",",date('d, m, Y',$schedulestart));
		$taskid = $myrow['taskid'];
		$scheduleid = $myrow['scheduleid'];
		$taskinterval = $myrow['taskinterval'];
		$taskarr = explode(',', $taskinterval);
		//print_r($taskarr);
		//echo '<br>';
	}
	//print_r($scheduledate);
	
	/*find holidays and display them*/
	$holidays = array();
	$query= "select * from holidays where holidaymonth = $month";
	//echo $query;
	$res = sql_query($query, $db);
	while($myrow = sql_fetch_assoc($res)){
		$holidayid = $myrow['holidayid'];
		$holidayname = $myrow['holidayname'];
		$holidayday = $myrow['holidayday'];
		
		$holidays[$holidayday] = $holidayname;
	}
	//print_r($holidays);
	
	
	?>
	<div id="calendartitledisplay">
		<h1>
			<a class="calendarskip left" onclick="showcalendar(<?echo $month-1;?>, <?echo $year;?>);">&lsaquo;</a>
				<?echo $calendartitledisplay;?>
			<a class="calendarskip right" onclick="showcalendar(<?echo $month+1;?>, <?echo $year;?>);">&rsaquo;</a>
		</h1>
	</div>
	<div class="topcalendarrow">
		<div class="calendartitle sunday">Sunday</div>
		<div class="calendartitle monday">Monday</div>
		<div class="calendartitle tuesday">Tuesday</div>
		<div class="calendartitle wednesday">Wednesday</div>
		<div class="calendartitle thursday">Thursday</div>
		<div class="calendartitle friday">Friday</div>
		<div class="calendartitle saturday">Saturday</div>
		<div class="clear"></div>
	</div>
	<div id="calendarcontainer" style="background:url('images/background-<?echo $month;?>.jpg');background-size:cover;">
	<?
	
	
	for($i=0; $i<6; $i++){
		?>
		<div class="calendarrow">
		<?
			for($j=0; $j<7; $j++){
			
			/*basic puzzle with borders*/
			if ($j%2== 0) {$puzzleybg = 'top';}
			else  $puzzleybg = 'bottom';
			//else $puzzleybg='none';
			if (($i%2 == 0) && ($j!=0)) {$puzzlexbg = 'left';}
			elseif ($j!=0) $puzzlexbg = 'right';
			else $puzzlexbg = 'none';
			
			/*sometimes has horizontal ipiece*/
			if ($i%2==0){
				if ($j%2==0){
					$puzzlexbg = 'none';
				}				
				else{
					$puzzlexbg = 'both';
				}
			}
			else {
				if ($j%2==1){
					$puzzlexbg = 'none';
				}
				else{
					$puzzlexbg = 'both';
				}
			}
			
			/*sometimes has vertical ipiece*/
			
			/*remove edge tabs*/
			
			if ($i==0){
				$puzzleybg = 'bottom';
			}
			elseif($i==5){
				$puzzleybg = 'top';
			}
			else{
				$puzzleybg = 'both';
			}
			
			if(($j==0)&&($i%2==1)){
				$puzzlexbg= 'left';
				$puzzleybg='none';
			}
			if (($j==6)&&($i%2==1)){
				$puzzlexbg = 'right';
				$puzzleybg = 'none';
				
			}
			
			
			
			
			
			
			
			$weekend='';
			$offfocus='';
			
			$indexname = date('N', mktime(0,0,0, $dateindex[1], $dateindex[0], $dateindex[2]));
			$indexmonth = date('M', mktime(0,0,0, $dateindex[1], $dateindex[0], $dateindex[2]));
			
			if(($indexname==6)||($indexname==7)){
				$weekend = 'weekend';
			}
			if($dateindex[1]!=$month){
				$offfocus = 'offfocus';
			}
			
			
			/*
			put into class if decide to go with background simulation
			puzzle<?echo $puzzleybg.$puzzlexbg?>
			*/
		?>
			<div class="calendaritem <?echo $weekend.' '.$offfocus;?>" onclick="listtasks(<?echo $dateindex[0];?>, <?echo $dateindex[1];?>, <?echo $dateindex[2];?>);">
				
				<?if ($j!=6){?>
					<div class="psuedoborder-xtopr">&nbsp;</div>
					<div class="psuedoborder-xbotr">&nbsp;</div>
					
				<?}?>
				<?if ($j!=0){?>
					<div class="psuedoborder-xtopl">&nbsp;</div>
					<div class="psuedoborder-xbotl">&nbsp;</div>
				<?}?>
				<?if ($i!=0){?>
					<div class="psuedoborder-yrtop">&nbsp;</div>
					<div class="psuedoborder-yltop">&nbsp;</div>
					
				<?}?>
				<?if ($i!=5){?>
					<div class="psuedoborder-yrbot">&nbsp;</div>
					<div class="psuedoborder-ylbot">&nbsp;</div>
				<?}?>
				<div class="displaytab">
					<label class="date"><?echo $dateindex[0];?></label>
					<?
					if ($offfocus!=''){
						?>
						<div class="monthreminder"><?echo $indexmonth;?></div>
						<?
					}
				?>
				<div class="clear"></div>
				</div>
				<?
				//echo $dateindex[0];
				$dateindex[0] = $dateindex[0]+0;
				if($holidays[$dateindex[0]] && ($dateindex[1]==$month)){
					?>
					<div class="holiday">
						<?
						echo $holidays[$dateindex[0]];
						?>
					</div>
					<?
				}
				
				if ($puzzlexbg == 'both'){
					?>
					<img class="puzzletab left" src="images/piece-left.svg" alt="puzzletab"/>
					<img class="puzzletab right" src="images/piece-right.svg" alt="puzzletab"/>
				<?}
				elseif ($puzzleybg == 'both'){
				?>
					<img class="puzzletab top" src="images/piece-top.svg" alt="puzzletab"/>
					<img class="puzzletab bottom" src="images/piece-bottom.svg" alt="puzzletab"/>
				<?}
				else{
					?>
					<img class="puzzletab <?echo $puzzleybg;?>" src="images/piece-<?echo $puzzleybg;?>.svg" alt="puzzletab"/>
					<img class="puzzletab <?echo $puzzlexbg;?>" src="images/piece-<?echo $puzzlexbg;?>.svg" alt="puzzletab"/>
				<?}?>
			</div>
			
			
		
		
		<?
		$dateindex = explode(",",date('d, m, Y', mktime(0,0,0, $dateindex[1], $dateindex[0]+1, $dateindex[2])));
		}//endfor
		?>
		<div class="clear"></div>
		</div>
		<?
	}//endfor
	?>
	</div>
	<?
	
		
}


