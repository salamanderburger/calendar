<?

include 'connect.php';
global $db;
$userid = 1; // users set to christine for now

date_default_timezone_set('America/Toronto');

?>


<!doctype html>
<html lang="en">
	<head>
		<title>Test Calendar</title>
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<meta charset="UTF-8">
		<link href="https://fonts.googleapis.com/css?family=Baloo+Bhaina" rel="stylesheet"> 
		<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">

	</head>
	
	<body>
		<?
		include 'functions/showcalendar.php';
		include 'functions/listtasks.php';
		
		?>
		<header>
			<span class="infotab">
			
			<?
			$query = "select * from users where users.userid = ".$userid;
			$res = sql_query($query, $db);
			$myrow = sql_fetch_assoc($res);
			$username = $myrow['userfname'].' '.$myrow['userlname'];
			
			echo 'logged in as '.$username.'.';
			?>
			<i id="usericon" class="fa fa-user" aria-hidden="true"></i>
			</span>
		</header>
		
		
		<!-- content of the calendar goes here -->
		<main>
			<div id="mainwrap">
				<div class="col1">
					<div id="calendar">	
					<?
					$currday = date('d');
					$currmonth = date('m');
					$curryear = date('Y');
					
					showcalendar($currmonth, $curryear);
					?>
					</div>
				</div>
				<div class="col2">
					<div id="tasks">
					<?
					listtasks($currday, $currmonth, $curryear);
					
					?>				
					</div>
					
				</div>
				<div class="clear"></div>
			</div>
		</main>
		
		<footer>
			 <script src="js/calendar.js"></script> 
		</footer>
	</body>
</html>
